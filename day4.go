package main

import (
	"bufio"
	"fmt"
	"strconv"
	"strings"

	mapset "github.com/deckarep/golang-set/v2"
)

func GetNumberOfOverlappingPairsFromString(input string) int {
	sets := CreateSetPairs(input)
	return CountOverlappingPairsInSetList(sets)
}

func CreateSetPairs(input string) [][]mapset.Set[int] {
	setlist := make([][]mapset.Set[int], 0)

	scanner := bufio.NewScanner(strings.NewReader(input))
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		s := strings.Trim(strings.TrimSpace(scanner.Text()), "\t")
		if s == "" {
			continue
		}

		parts := strings.Split(s, ",")
		setA := createSetRange(extractSetRanges(parts[0]))
		setB := createSetRange(extractSetRanges(parts[1]))

		setlist = append(setlist, []mapset.Set[int]{setA, setB})
	}

	return setlist
}

func createSetRange(lower, upper int) mapset.Set[int] {
	set := mapset.NewSet[int]()
	for i := lower; i <= upper; i++ {
		set.Add(int(i))
	}

	return set
}

func extractSetRanges(rangeDesc string) (int, int) {
	rng := strings.Split(rangeDesc, "-")
	lower, _ := strconv.ParseInt(rng[0], 10, 32)
	upper, _ := strconv.ParseInt(rng[1], 10, 32)

	return int(lower), int(upper)
}

func CountOverlappingPairsInSetList(sets [][]mapset.Set[int]) int {
	n := 0
	for _, setpair := range sets {
		/*a := setpair[0].ToSlice()
		b := setpair[1].ToSlice()
		sort.Sort(sort.IntSlice(a))
		sort.Sort(sort.IntSlice(b))

		minA := a[0]
		maxA := a[len(a)-1]

		minB := b[0]
		maxB := b[len(b)-1]

		if (minB >= minA && maxB <= maxA) || (minA >= minB && maxA <= maxB) {
			n++
		}*/

		if setpair[0].Intersect(setpair[1]).Cardinality() > 0 {
			n++
		}
	}
	return n
}

type Day4Result struct {
	number int
}

func (r Day4Result) Result() string {
	return fmt.Sprintf("number=%d", r.number)
}

func Day4Runner(input string) dayResult {
	r := Day4Result{
		number: GetNumberOfOverlappingPairsFromString(input),
	}
	return r
}
