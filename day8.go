package main

import (
	"aoc-2022/helper"
	"fmt"
	"strconv"
	"strings"
)

type Day8Result string

func (d Day8Result) Result() string {
	fm := NewForestMapFromString(string(d))
	return fmt.Sprintf("visibleTrees=%d highestScenicScore=%d", fm.VisibleTrees(), fm.HighestScenicScore())
}

func Day8Runner(input string) dayResult {
	return Day8Result(input)
}

func NewForestMapFromString(strMap string) ForestMap {
	fm := ForestMap{}

	helper.ProcessStringLineByLine(strMap, func(line string) {
		line = strings.Trim(strings.TrimSpace(line), "\t")
		if line == "" {
			return
		}

		fm = append(fm, make([]int64, len(line)))
		for i := 0; i < len(line); i++ {
			height, err := strconv.ParseInt(string(line[i]), 10, 64)
			if err != nil {
				panic(err)
			}
			fm[len(fm)-1][i] = height
		}
	})

	return fm
}

type ForestMap [][]int64

func (fm ForestMap) VisibleTrees() int {
	count := 0
	for y := 0; y < len(fm); y++ {
		for x := 0; x < len(fm[y]); x++ {
			if fm.isTreeVisible(x, y) {
				count++
			}
		}
	}
	return count
}

func (fm ForestMap) Width() int {
	if len(fm) == 0 {
		return 0
	}
	return len(fm[0])
}

func (fm ForestMap) Height() int {
	return len(fm)
}

func (fm ForestMap) isTreeVisible(x, y int) bool {
	if fm.isBorderTree(x, y) {
		return true
	}

	if fm.checkLefLineOfSight(x, y) || fm.checkRightLineOfSight(x, y) ||
		fm.checkTopLineOfSight(x, y) || fm.checkLowerLineOfSight(x, y) {
		return true
	}

	return false
}

func (fm ForestMap) checkLefLineOfSight(x, y int) bool {
	aX := x

	for x--; x >= 0; x-- {
		if !fm.isTreeHigher(aX, y, x, y) {
			return false
		}
	}

	return true
}

func (fm ForestMap) checkRightLineOfSight(x, y int) bool {
	aX := x

	for x++; x < len(fm[y]); x++ {
		if !fm.isTreeHigher(aX, y, x, y) {
			return false
		}
	}

	return true
}

func (fm ForestMap) checkTopLineOfSight(x, y int) bool {
	aY := y

	for y--; y >= 0; y-- {
		if !fm.isTreeHigher(x, aY, x, y) {
			return false
		}
	}

	return true
}

func (fm ForestMap) checkLowerLineOfSight(x, y int) bool {
	aY := y

	for y++; y < len(fm); y++ {
		if !fm.isTreeHigher(x, aY, x, y) {
			return false
		}
	}

	return true
}

func (fm ForestMap) isTreeHigher(ax, ay, bx, by int) bool {
	return fm[ay][ax] > fm[by][bx]
}

func (fm ForestMap) isBorderTree(x, y int) bool {
	return y == 0 || y == len(fm)-1 || x == 0 || x == len(fm[0])-1
}

func (fm ForestMap) HighestScenicScore() int {
	score := 0
	for y := 0; y < len(fm); y++ {
		for x := 0; x < len(fm[y]); x++ {
			if fm.scenicScoreOf(x, y) > score {
				score = fm.scenicScoreOf(x, y)
			}
		}
	}
	return score
}

func (fm ForestMap) scenicScoreOf(x, y int) int {
	if fm.isBorderTree(x, y) {
		return 0
	}

	return fm.treesVisibleBelow(x, y) * fm.treesVisibleLeft(x, y) * fm.treesVisibleRight(x, y) * fm.treesVisibleOnTop(x, y)
}

func (fm ForestMap) treesVisibleOnTop(x, y int) int {
	aY := y
	count := 0
	for y--; y >= 0; y-- {
		count++
		if !fm.isTreeHigher(x, aY, x, y) {
			break
		}
	}

	return count
}

func (fm ForestMap) treesVisibleBelow(x, y int) int {
	aY := y
	count := 0
	for y++; y < len(fm); y++ {
		count++
		if !fm.isTreeHigher(x, aY, x, y) {
			break
		}
	}

	return count
}

func (fm ForestMap) treesVisibleLeft(x, y int) int {
	aX := x
	count := 0
	for x--; x >= 0; x-- {
		count++
		if !fm.isTreeHigher(aX, y, x, y) {
			break
		}
	}

	return count
}

func (fm ForestMap) treesVisibleRight(x, y int) int {
	aX := x
	count := 0
	for x++; x < len(fm[y]); x++ {
		count++
		if !fm.isTreeHigher(aX, y, x, y) {
			break
		}
	}

	return count
}
