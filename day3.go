package main

import (
	"bufio"
	"fmt"
	"strings"
)

func CaclucalteSumOfItemsInEachRucksack(packlist string) int {
	sum := 0

	scanner := bufio.NewScanner(strings.NewReader(packlist))
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		if strings.TrimSpace(scanner.Text()) == "" {
			continue
		}

		rucksack := CreateRucksackFromString(scanner.Text())

		sum += Day3ConvertRuneToValue(rucksack.GetDuplicateItem())
	}

	return sum
}

type Day3Rucksack struct {
	compartment1 string
	compartment2 string
}

func (r Day3Rucksack) Packlist() string {
	return fmt.Sprintf("%s%s", r.compartment1, r.compartment2)
}

func (rs Day3Rucksack) Contains(r rune) bool {
	return strings.Contains(rs.Packlist(), string(r))
}

func CreateRucksackFromString(contents string) Day3Rucksack {
	l := len(contents) / 2

	return Day3Rucksack{
		compartment1: contents[0:l],
		compartment2: contents[l:],
	}
}

func (r Day3Rucksack) Compartment1() string {
	return r.compartment1
}

func (r Day3Rucksack) Compartment2() string {
	return r.compartment2
}

func (r Day3Rucksack) GetDuplicateItem() rune {
	for _, a := range r.compartment1 {
		for _, b := range r.compartment2 {
			if a == b {
				return a
			}
		}
	}
	return 0
}

func Day3ConvertRuneToValue(r rune) int {
	v := int(r)
	if v >= 97 {
		return v - 96
	} else {
		return v - 38
	}
}

func (r Day3Rucksack) CalculateSumOf(a, b rune) int {
	return Day3ConvertRuneToValue(a) + Day3ConvertRuneToValue(b)
}

type Day3Result struct {
	sum      int
	badgeSum int
}

func (r Day3Result) Result() string {
	return fmt.Sprintf("sum=%d sumOfBadges=%d", r.sum, r.badgeSum)
}

func Day3Runner(input string) dayResult {
	r := Day3Result{
		sum:      CaclucalteSumOfItemsInEachRucksack(input),
		badgeSum: CalculateSumOfBadgeItemGroups(input),
	}
	return r
}

func CalculateSumOfBadgeItemGroups(packlist string) int {
	sum := 0
	badgeGroups := GetBadgeGroupsFromRucksacks(packlist)
	for _, item := range badgeGroups {
		sum += Day3ConvertRuneToValue(item)
	}
	return sum
}

func GetBadgeGroupsFromRucksacks(packlist string) []rune {
	rucksacks := convertPacklistToRucksackList(packlist)
	badges := make([]rune, 0)

	for i := 0; i < len(rucksacks); i += 3 {
		badge := findBadgeForRucksacks(rucksacks[i : i+3])
		badges = append(badges, badge)
	}

	return badges
}

func convertPacklistToRucksackList(packlist string) []Day3Rucksack {
	rucksacks := make([]Day3Rucksack, 0)

	scanner := bufio.NewScanner(strings.NewReader(packlist))
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		s := strings.Trim(strings.TrimSpace(scanner.Text()), "\t")
		if s == "" {
			continue
		}

		rucksacks = append(rucksacks, CreateRucksackFromString(s))
	}

	return rucksacks
}

func findBadgeForRucksacks(rucksacks []Day3Rucksack) rune {
	for _, r := range rucksacks[0].Packlist() {
		if rucksacks[1].Contains(r) && rucksacks[2].Contains(r) {
			return r
		}
	}
	return -1
}
