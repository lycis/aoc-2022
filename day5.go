package main

import (
	"aoc-2022/helper"
	"fmt"
	"strings"
)

type Day5Puzzle struct {
	stacks map[int][]string
}

func (p Day5Puzzle) Result() string {
	return p.GetTopOfAllCrateOfStacks()
}

func (p Day5Puzzle) GetTopOfAllCrateOfStacks() string {
	stacks := make([]string, len(p.stacks))
	for stack := range p.stacks {
		stacks[stack] = p.GetTopCrateOfStack(stack)
	}

	out := ""
	for i := range stacks {
		out += stacks[i]
	}

	return out
}

func (p *Day5Puzzle) PutCrate(id string, stack int) {
	if _, ok := p.stacks[stack]; !ok {
		p.stacks[stack] = make([]string, 0)
	}
	p.stacks[stack] = append(p.stacks[stack], id)
}

func (p *Day5Puzzle) PutCrateToBack(id string, stack int) {
	if _, ok := p.stacks[stack]; !ok {
		p.stacks[stack] = make([]string, 0)
	}
	p.stacks[stack] = append([]string{id}, p.stacks[stack]...)
}

func (p Day5Puzzle) GetTopCrateOfStack(stack int) string {
	return p.stacks[stack][len(p.stacks[stack])-1]
}

func (p *Day5Puzzle) MoveCrate(from, to int) {
	crate := p.stacks[from][len(p.stacks[from])-1]
	p.stacks[from] = p.stacks[from][:len(p.stacks[from])-1]

	p.PutCrate(crate, to)
}

func (p *Day5Puzzle) MoveCrateStack(stackSize, from, to int) {
	crates := p.stacks[from][len(p.stacks[from])-stackSize:]
	p.stacks[from] = p.stacks[from][:len(p.stacks[from])-stackSize]

	p.stacks[to] = append(p.stacks[to], crates...)
}

func NewDay5Puzzle() Day5Puzzle {
	return Day5Puzzle{
		stacks: make(map[int][]string),
	}
}

func Day5RearrangeCratesAccordingToInstructions(input string) Day5Puzzle {
	puzzle := NewDay5Puzzle()

	helper.ProcessStringLineByLine(input, func(line string) {
		if strings.Contains(line, "[") {
			stack := 0
			for i := 0; i < len(line); {

				var chunkSize int
				if i+4 > len(line) {
					chunkSize = 3
				} else {
					chunkSize = 4
				}

				crate := strings.TrimSpace(line[i : i+chunkSize])
				var crateId string
				if crate != "" {
					crateId = strings.Trim(crate, "[]")
					puzzle.PutCrateToBack(crateId, stack)
				}
				stack++
				i += chunkSize
			}
		} else if strings.HasPrefix(strings.Trim(strings.TrimSpace(line), "\t"), "move") {
			amnt, from, to := TokenizeMoveCrateCommand(strings.Trim(strings.TrimSpace(line), "\t"))
			puzzle.MoveCrateStack(amnt, from-1, to-1)
		}
	})

	return puzzle
}

func TokenizeMoveCrateCommand(line string) (int, int, int) {
	var num, from, to int
	n, err := fmt.Sscanf(line, "move %d from %d to %d", &num, &from, &to)
	if n != 3 || err != nil {
		panic(err)
	}

	return num, from, to
}

func Day5Runner(input string) dayResult {
	return Day5RearrangeCratesAccordingToInstructions(input)
}
