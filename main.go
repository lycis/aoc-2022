package main

import (
	"fmt"
	"io"
	"os"
)

type dayResult interface {
	Result() string
}

type dayFunc func(string) dayResult

func RunDay(df dayFunc, day int) {
	fmt.Printf("-- Day %d: ", day)

	f, err := os.Open(fmt.Sprintf("./input/%d.txt", day))
	if err != nil {
		fmt.Printf("error - %s\n", err.Error())
		return
	}
	defer f.Close()

	input, err := io.ReadAll(f)
	if err != nil {
		fmt.Printf("error - %s\n", err.Error())
		return
	}

	fmt.Printf("%s\n", df(string(input[:])).Result())
}

func main() {
	RunDay(Day1Runner, 1)
	RunDay(Day2Runner, 2)
	RunDay(Day3Runner, 3)
	RunDay(Day4Runner, 4)
	RunDay(Day5Runner, 5)
	RunDay(Day6Runner, 6)
	RunDay(Day7Runner, 7)
	RunDay(Day8Runner, 8)
}
