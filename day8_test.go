package main

import (
	"testing"

	"github.com/fluentassert/verify"
)

func TestDetectVisibleTreesBasedOnHeightOfSurroundingTrees(t *testing.T) {
	area := NewForestMapFromString(`30373
									25512
									65332
									33549
									35390`)
	verify.Number(area.VisibleTrees()).Equal(21).Assert(t)
}

func TestSmallTreeSurroundendByHighTreesIsNotCounted(t *testing.T) {
	area := NewForestMapFromString(`33333
									34443
									34143
									34443
									33333`)
	verify.Number(area.VisibleTrees()).Equal(24).Assert(t)
}

func TestForestMapHasHeightAndWidthOfInputMap(t *testing.T) {
	area := NewForestMapFromString(`111
	                                111
									111
									111`)

	verify.Number(area.Width()).Equal(3).Assert(t)
	verify.Number(area.Height()).Equal(4).Assert(t)
}

func TestEmptyForestMapHasHeightAndWidthZero(t *testing.T) {
	area := NewForestMapFromString(``)

	verify.Number(area.Width()).Equal(0).Assert(t)
	verify.Number(area.Height()).Equal(0).Assert(t)
}

func TestBorderTreesAreAlwaysVisible(t *testing.T) {
	area := NewForestMapFromString(`111
	                                101
									111`)

	verify.True(area.isTreeVisible(0, 0)).Assert(t)
	verify.True(area.isTreeVisible(1, 0)).Assert(t)
	verify.True(area.isTreeVisible(2, 0)).Assert(t)
	verify.True(area.isTreeVisible(0, 1)).Assert(t)
	verify.True(area.isTreeVisible(2, 1)).Assert(t)
	verify.True(area.isTreeVisible(0, 2)).Assert(t)
	verify.True(area.isTreeVisible(1, 2)).Assert(t)
	verify.True(area.isTreeVisible(2, 2)).Assert(t)
}

func TestTreeIsVisibleWhenItIsHigherThanThanTreesOnLeftSide(t *testing.T) {
	area := NewForestMapFromString(`111
	                                151
									111`)

	verify.True(area.checkLefLineOfSight(1, 1)).Assert(t)
}

func TestTreeIsNotVisibleWhenAnotherTreeOnTheLiftSideIsHigher(t *testing.T) {
	area := NewForestMapFromString(`11111
	                                81151
									11111`)

	verify.False(area.checkLefLineOfSight(3, 1)).Assert(t)
}

func TestTreeIsNotVisibleWhenAnotherTreeOnTheRightSideIsHigher(t *testing.T) {
	area := NewForestMapFromString(`11111
	                                11158
									11111`)

	verify.False(area.checkRightLineOfSight(3, 1)).Assert(t)
}

func TestTreeIsVisibleWhenNoTreeOnTheRightSideIsHigher(t *testing.T) {
	area := NewForestMapFromString(`11111
	                                11151
									11111`)

	verify.True(area.checkRightLineOfSight(3, 1)).Assert(t)
}

func TestTreeIsNotVisibleWhenAnotherTreeOnTopSideIsHigher(t *testing.T) {
	area := NewForestMapFromString(`11181
	                                11151
									11111`)

	verify.False(area.checkTopLineOfSight(3, 1)).Assert(t)
}

func TestTreeIsVisibleWhenNoOtherTreeOnTopSideIsHigher(t *testing.T) {
	area := NewForestMapFromString(`11111
	                                11151
									11111`)

	verify.True(area.checkTopLineOfSight(3, 1)).Assert(t)
}

func TestTreeIsNotVisibleWhenAnotherTreeOnLowerSideIsHigher(t *testing.T) {
	area := NewForestMapFromString(`11111
	                                11151
									11181`)

	verify.False(area.checkLowerLineOfSight(3, 1)).Assert(t)
}

func TestTreeIsVisibleWhenNoOtherTreeOnLowerSideIsHigher(t *testing.T) {
	area := NewForestMapFromString(`11111
	                                11151
									11111`)

	verify.True(area.checkLowerLineOfSight(3, 1)).Assert(t)
}

func TestATreeIsVisibleWhenThereIsNoHigherTreeSurroundingIt(t *testing.T) {
	area := NewForestMapFromString(`11111
	                                11511
									11111`)

	verify.True(area.isTreeVisible(2, 1)).Assert(t)
}

func TestTreeIsNotVisibleWhenSurroundedByHigherTrees(t *testing.T) {
	area := NewForestMapFromString(`11811
	                                18581
									11811`)

	verify.False(area.isTreeVisible(2, 1)).Assert(t)
}

func TestATreeOnTheBorderOfTheAreaIsABorderTree(t *testing.T) {
	area := NewForestMapFromString(`11
	                                11`)

	verify.True(area.isBorderTree(0, 0)).Assert(t)
	verify.True(area.isBorderTree(1, 0)).Assert(t)
	verify.True(area.isBorderTree(0, 1)).Assert(t)
	verify.True(area.isBorderTree(1, 1)).Assert(t)
}

func TestScenicScoreForBorderTreesIsZero(t *testing.T) {
	area := NewForestMapFromString(`11
	                                11`)

	verify.Number(area.scenicScoreOf(0, 0)).Equal(0).Assert(t)
	verify.Number(area.scenicScoreOf(1, 0)).Equal(0).Assert(t)
	verify.Number(area.scenicScoreOf(0, 1)).Equal(0).Assert(t)
	verify.Number(area.scenicScoreOf(1, 1)).Equal(0).Assert(t)
}

func TestScenicScoreForTreeIsNumberOfVisibleTreesOnEachSideMultiplied(t *testing.T) {
	area := NewForestMapFromString(`30373
									25512
									65332
									33549
									35390`)

	verify.Number(area.scenicScoreOf(2, 1)).Equal(4).Assert(t)
	verify.Number(area.scenicScoreOf(2, 3)).Equal(8).Assert(t)
}

func TestHighestScenicScoreIsGiven(t *testing.T) {
	area := NewForestMapFromString(`30373
									25512
									65332
									33549
									35390`)

	verify.Number(area.HighestScenicScore()).Equal(8).Assert(t)
}

func TestTopViewingDistanceIsNumberOfSeenTreesUntilBorderIfNoTreeBlockSight(t *testing.T) {
	area := NewForestMapFromString(`111
	                                111
									121
									111`)

	verify.Number(area.treesVisibleOnTop(1, 2)).Equal(2).Assert(t)
}

func TestTopViewingDistanceIsNumberOfSeenTreesUntilATreeBlockSight(t *testing.T) {
	area := NewForestMapFromString(`111
	                                131
									121
									111`)

	verify.Number(area.treesVisibleOnTop(1, 2)).Equal(1).Assert(t)
}

func TestLowerViewingDistanceIsNumberOfSeenTreesUntilBorderIfNoTreeBlockSight(t *testing.T) {
	area := NewForestMapFromString(`111
	                                121
									111
									111`)

	verify.Number(area.treesVisibleBelow(1, 1)).Equal(2).Assert(t)
}

func TestLowerViewingDistanceIsNumberOfSeenTreesUntilATreeBlockSight(t *testing.T) {
	area := NewForestMapFromString(`111
	                                121
									131
									111`)

	verify.Number(area.treesVisibleBelow(1, 1)).Equal(1).Assert(t)
}

func TestLeftViewingDistanceIsNumberOfSeenTreesUntilBorderIfNoTreeBlockSight(t *testing.T) {
	area := NewForestMapFromString(`1111
	                                1121
									1111
									1111`)

	verify.Number(area.treesVisibleLeft(2, 1)).Equal(2).Assert(t)
}

func TestLeftViewingDistanceIsNumberOfSeenTreesUntilATreeBlockSight(t *testing.T) {
	area := NewForestMapFromString(`11111
									12121
									11111
									11111`)

	verify.Number(area.treesVisibleLeft(3, 1)).Equal(2).Assert(t)
}

func TestRightViewingDistanceIsNumberOfSeenTreesUntilBorderIfNoTreeBlockSight(t *testing.T) {
	area := NewForestMapFromString(`1111
	                                1211
									1111
									1111`)

	verify.Number(area.treesVisibleRight(1, 1)).Equal(2).Assert(t)
}

func TestRightViewingDistanceIsNumberOfSeenTreesUntilATreeBlockSight(t *testing.T) {
	area := NewForestMapFromString(`11111
									12121
									11111
									11111`)

	verify.Number(area.treesVisibleRight(1, 1)).Equal(2).Assert(t)
}
