package main

import (
	"testing"

	mapset "github.com/deckarep/golang-set/v2"
	"github.com/fluentassert/verify"
)

func TestGetNumberOfOverlappingSets(t *testing.T) {
	verify.Number(GetNumberOfOverlappingPairsFromString(`
	2-4,6-8
	2-3,4-5
	5-7,7-9
	2-8,3-7
	6-6,4-6
	2-6,4-8
	1-10,6-6
	`)).Equal(3).Assert(t)
}

func TestCreateSetPairListFromString(t *testing.T) {

	setPairs := CreateSetPairs(`
	2-4,6-8
	`)

	verify.Number(len(setPairs)).Equal(1).Assert(t)

	pair := setPairs[0]
	verify.Number(len(pair)).Equal(2).Assert(t)
	verify.True(pair[0].Contains(2, 3, 4)).Assert(t)
	verify.True(pair[1].Contains(6, 7, 8)).Assert(t)

}

func TestFindOverlappingPairsInSetListWhenPairBinPairA(t *testing.T) {
	pairlist := append(make([][]mapset.Set[int], 0), []mapset.Set[int]{mapset.NewSet(1, 2, 3, 4), mapset.NewSet(3, 4)})

	verify.Number(CountOverlappingPairsInSetList(pairlist)).Equal(1).Assert(t)
}

func TestFindOverlappingPairsInSetListWhenPairBIsASingleNumber(t *testing.T) {
	pairlist := append(make([][]mapset.Set[int], 0), []mapset.Set[int]{mapset.NewSet(1, 2, 3, 4), mapset.NewSet(4)})

	verify.Number(CountOverlappingPairsInSetList(pairlist)).Equal(1).Assert(t)
}

func TestFindOverlappingPairsInSetListWhenPairAinPairB(t *testing.T) {
	pairlist := append(make([][]mapset.Set[int], 0), []mapset.Set[int]{mapset.NewSet(2, 3), mapset.NewSet(1, 2, 3, 4)})

	verify.Number(CountOverlappingPairsInSetList(pairlist)).Equal(1).Assert(t)
}

func TestFindOverlappingPairsInSetListAreNotSubsets(t *testing.T) {
	pairlist := append(make([][]mapset.Set[int], 0), []mapset.Set[int]{mapset.NewSet(2, 3), mapset.NewSet(11, 12)})

	verify.Number(CountOverlappingPairsInSetList(pairlist)).Equal(0).Assert(t)
}

func TestFindOverlappingPairsForMultiplePairs(t *testing.T) {
	pairlist := append(
		append(
			make([][]mapset.Set[int], 0), []mapset.Set[int]{mapset.NewSet(2, 3), mapset.NewSet(1, 2, 3, 4)}),
		[]mapset.Set[int]{mapset.NewSet(7, 8, 9), mapset.NewSet(8, 9)})

	verify.Number(CountOverlappingPairsInSetList(pairlist)).Equal(2).Assert(t)
}
