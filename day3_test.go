package main

import (
	"strings"
	"testing"

	"github.com/fluentassert/verify"
)

func TestRucksackSortsHalfTheItemsInTwoCompartments(t *testing.T) {
	rucksack := CreateRucksackFromString("vJrwpWtwJgWrhcsFMMfFFhFp")
	verify.String(rucksack.Compartment1()).Equal("vJrwpWtwJgWr").Assert(t)
	verify.String(rucksack.Compartment2()).Equal("hcsFMMfFFhFp").Assert(t)
}

func TestRucksackSortsSmallestRucksackInTwoCompartments(t *testing.T) {
	rucksack := CreateRucksackFromString("vv")
	verify.String(rucksack.Compartment1()).Equal("v").Assert(t)
	verify.String(rucksack.Compartment2()).Equal("v").Assert(t)
}

func TestCaclculateSumOfDoublePackedItemsInEachRucksack(t *testing.T) {
	sum := CaclucalteSumOfItemsInEachRucksack(`vJrwpWtwJgWrhcsFMMfFFhFp
	jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
	PmmdzqPrVvPwwTWBwg
	wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
	ttgJtRGJQctTZtZT
	CrZsJsPPZsGzwwsLwLmpwMDw`)

	verify.Number(sum).Equal(157).Assert(t)
}

func TestCaclculateSumOfDoublePackedItemsInASingleRuckack(t *testing.T) {
	sum := CaclucalteSumOfItemsInEachRucksack(`vJrwpWtwJgWrhcsFMMfFFhFp`)

	verify.Number(sum).Equal(16).Assert(t)
}

func TestFindDuplicateItemInRucksack(t *testing.T) {
	rucksack := CreateRucksackFromString("vJrwpWtwJgWrhcsFMMfFFhFp")
	verify.String(string(rucksack.GetDuplicateItem())).Equal("p").Assert(t)
}

func TestFindDuplaceItemInVaryingRucksackSizes(t *testing.T) {
	rucksack := CreateRucksackFromString("vv")
	verify.String(string(rucksack.GetDuplicateItem())).Equal("v").Assert(t)

	rucksack = CreateRucksackFromString("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL")
	verify.String(string(rucksack.GetDuplicateItem())).Equal("L").Assert(t)
}

func TestCalculateSumOfTwoItems(t *testing.T) {
	rucksack := CreateRucksackFromString("vv")

	lower := []rune{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'}
	for i, character := range lower {
		upper := rune(strings.ToUpper(string(character))[0])
		v1 := int(lower[i]) - 96
		v2 := int(upper) - 38
		result := v1 + v2
		verify.Number(rucksack.CalculateSumOf(lower[i], upper)).Equal(result).Assert(t)
	}
}

func TestCalculateSumOfBadgeGroups(t *testing.T) {
	sum := CalculateSumOfBadgeItemGroups(`
	vJrwpWtwJgWrhcsFMMfFFhFp
	jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
	PmmdzqPrVvPwwTWBwg
	wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
	ttgJtRGJQctTZtZT
	CrZsJsPPZsGzwwsLwLmpwMDw`)

	verify.Number(sum).Equal(70).Assert(t)
}

func TestGetBadgeGroupsFromRucksacks(t *testing.T) {
	badgeGroups := GetBadgeGroupsFromRucksacks(`
	vJrwpWtwJgWrhcsFMMfFFhFp
	jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
	PmmdzqPrVvPwwTWBwg
	wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
	ttgJtRGJQctTZtZT
	CrZsJsPPZsGzwwsLwLmpwMDw`)

	verify.Number(len(badgeGroups)).Equal(2).Assert(t)
	verify.Slice(badgeGroups).Contain('r').Assert(t)
	verify.Slice(badgeGroups).Contain('Z').Assert(t)
}

func TestConvertPacklistToRucksackList(t *testing.T) {
	rucksacks := convertPacklistToRucksackList(`
	vJrwpWtwJgWrhcsFMMfFFhFp
	jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
	PmmdzqPrVvPwwTWBwg
	`)

	verify.Number(len(rucksacks)).Equal(3).Assert(t)
}

func TestGetBadgeGroupFromGroupOfThreeRucksacks(t *testing.T) {
	rucksacks := []Day3Rucksack{
		CreateRucksackFromString("vJrwpWtwJgWrhcsFMMfFFhFp"),
		CreateRucksackFromString("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL"),
		CreateRucksackFromString("PmmdzqPrVvPwwTWBwg"),
	}

	badge := findBadgeForRucksacks(rucksacks)

	verify.Number(badge).Equal('r').Assert(t)
}

func TestRucksackReturnsPacklistFromContents(t *testing.T) {
	rucksack := CreateRucksackFromString("abcdefg")
	verify.String(rucksack.Packlist()).Equal("abcdefg").Assert(t)
}

func TestRucksackContainsItemInPacklist(t *testing.T) {
	rucksack := CreateRucksackFromString("abcdefg")
	verify.True(rucksack.Contains('c')).Assert(t)
}
