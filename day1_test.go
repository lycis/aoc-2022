package main

import (
	"testing"

	"github.com/fluentassert/verify"
)

func TestD1FindLargestBucketReturnsNrOfBucketWithBiggestNumber(t *testing.T) {
	puzzle := NewDay1Puzzle()
	puzzle.openBucket()
	puzzle.AddToBucket(1)

	puzzle.openBucket()
	puzzle.AddToBucket(5)

	puzzle.openBucket()
	puzzle.AddToBucket(4)

	verify.Number(puzzle.FindLargestBucket()).Equal(1)
}

func TestD1AddToBucketIncreasesCurrentBucketByBumberGiven(t *testing.T) {
	puzzle := NewDay1Puzzle()
	puzzle.openBucket()
	puzzle.AddToBucket(100)
	verify.Number(puzzle.buckets[puzzle.currentBucket]).Equal(100)
}

func TestD1OpenBucketCreatesANewBucketWithSumZero(t *testing.T) {
	puzzle := NewDay1Puzzle()
	puzzle.openBucket()
	verify.Number(puzzle.currentBucket).Equal(0).Assert(t)
	verify.Number(len(puzzle.buckets)).Equal(1)
	verify.Number(puzzle.buckets[0]).Equal(0)
}

func TestD1EmptyLineCreatesNewBucket(t *testing.T) {
	puzzle := Day1(
		`1
	 2
	 
	 3
	 4
	 5
	 
	 6
	 `)
	verify.Number(len(puzzle.buckets)).Equal(4).Assert(t)
}

func TestD1NumbersAreSummedOpInTheirBucket(t *testing.T) {
	puzzle := Day1(
		`1
	 2
	 
	 3
	 4
	 5
	 
	 6
	 `)

	verify.Number(puzzle.buckets[0]).Equal(3).Assert(t)
	verify.Number(puzzle.buckets[1]).Equal(12).Assert(t)
	verify.Number(puzzle.buckets[2]).Equal(6).Assert(t)
}

func TestD1ReturnsLargestBucketFromInputFile(t *testing.T) {
	puzzle := Day1(`
	3
	3
	3
	
	1
	1
	1
	
	5
	5
	5
	
	10
	`)

	verify.Number(puzzle.FindLargestBucket()).Equal(3).Assert(t)
}

func TestCalculateTopThreeCalories(t *testing.T) {
	puzzle := Day1(`
	3
	3
	3
	
	1
	1
	1
	
	5
	5
	5
	
	10
	`)
	verify.Number(puzzle.CalcTopThree()).Equal(34).Assert(t)
}

func TestD1LastLineCreatesNewBucket(t *testing.T) {
	puzzle := Day1("1")
	verify.Number(len(puzzle.buckets)).Equal(1).Assert(t)
}
