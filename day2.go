package main

import (
	"bufio"
	"fmt"
	"strings"
)

const DAY2_OPPONENT_ROCK = "A"
const DAY2_OPPONENT_PAPER = "B"
const DAY2_OPPONENT_SICCORS = "C"

const DAY2_SELF_ROCK = "X"
const DAY2_SELF_PAPER = "Y"
const DAY2_SELF_SICCORS = "Z"

const (
	DAY2_LOSS = 0
	DAY2_WIN  = 1
	DAY2_DRAW = 2
)

type aocDay2 struct {
	score int
}

func (p *aocDay2) SumScoreFromString(input string) int {
	p.score = 0

	scanner := bufio.NewScanner(strings.NewReader(input))
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		if strings.TrimSpace(scanner.Text()) == "" {
			continue
		}
		p.score += p.CalculateRoundFromString(scanner.Text())
	}

	return p.score
}

func (p aocDay2) CalculateRoundFromString(input string) int {
	input = strings.TrimSpace(input)
	parts := strings.Split(input, " ")
	intent := p.IntentToResult(parts[1])
	myPick := p.FigureOutWhatToChoose(parts[0], intent)

	return p.CalculateRound(parts[0], myPick)
}

func (p aocDay2) CalculateRound(opponent string, self string) int {
	points := 0

	roundResult := p.EvaluateRound(opponent, self)
	switch roundResult {
	case DAY2_WIN:
		points += 6
	case DAY2_LOSS:
		points += 0
	case DAY2_DRAW:
		points += 3
	}

	switch self {
	case DAY2_SELF_PAPER:
		points += 2
	case DAY2_SELF_SICCORS:
		points += 3
	case DAY2_SELF_ROCK:
		points += 1
	}

	return points
}

func (p aocDay2) EvaluateRound(opponent string, self string) int {
	if (opponent == DAY2_OPPONENT_SICCORS && self == DAY2_SELF_SICCORS) ||
		(opponent == DAY2_OPPONENT_ROCK && self == DAY2_SELF_ROCK) ||
		(opponent == DAY2_OPPONENT_PAPER && self == DAY2_SELF_PAPER) {
		return DAY2_DRAW
	}

	if (opponent == DAY2_OPPONENT_SICCORS && self == DAY2_SELF_ROCK) ||
		(opponent == DAY2_OPPONENT_ROCK && self == DAY2_SELF_PAPER) ||
		(opponent == DAY2_OPPONENT_PAPER && self == DAY2_SELF_SICCORS) {
		return DAY2_WIN
	} else if (opponent == DAY2_OPPONENT_ROCK && self == DAY2_SELF_SICCORS) ||
		(opponent == DAY2_OPPONENT_PAPER && self == DAY2_SELF_ROCK) ||
		(opponent == DAY2_OPPONENT_SICCORS && self == DAY2_SELF_PAPER) {
		return DAY2_LOSS
	}
	return -1
}

func (p aocDay2) IntentToResult(intent string) int {
	switch intent {
	case "X":
		return DAY2_LOSS
	case "Y":
		return DAY2_DRAW
	case "Z":
		return DAY2_WIN
	}
	return -1
}

func (p aocDay2) FigureOutWhatToChoose(opponent string, intent int) string {
	strategy := map[string][]string{
		DAY2_OPPONENT_PAPER:   {DAY2_SELF_ROCK, DAY2_SELF_SICCORS, DAY2_SELF_PAPER},
		DAY2_OPPONENT_SICCORS: {DAY2_SELF_PAPER, DAY2_SELF_ROCK, DAY2_SELF_SICCORS},
		DAY2_OPPONENT_ROCK:    {DAY2_SELF_SICCORS, DAY2_SELF_PAPER, DAY2_SELF_ROCK},
	}
	return strategy[opponent][intent]
}

func NewDay2Puzzle() aocDay2 {
	return aocDay2{}
}

func (p aocDay2) Result() string {
	return fmt.Sprintf("score=%d", p.score)
}

func Day2Runner(input string) dayResult {
	puzzle := NewDay2Puzzle()
	puzzle.SumScoreFromString(input)
	return puzzle
}
