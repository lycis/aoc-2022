package main

import (
	"testing"

	"github.com/fluentassert/verify"
)

func TestGetTotalSizeOfDirectoryWithSizeLowerThan100k(t *testing.T) {
	verify.Number(CalculateTotalSizeOfDirectoriesFromInput(`
	$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k`)).Equal(95437).Assert(t)
}

func TestSumUpASingleDirectoryReturnsSizeOfThatDirectory(t *testing.T) {
	dirA := CreateDirectory("a")
	dirA.AddFile("a", 500)
	dirs := []*directory{
		&dirA,
	}
	verify.Number(sumUpDirectorySizes(dirs)).Equal(dirA.Size()).Assert(t)
}

func TestSumUpTwoDirectoriesReturnsTheirSum(t *testing.T) {
	dirA := CreateDirectory("a")
	dirA.AddFile("x", 500)
	dirB := CreateDirectory("b")
	dirB.AddFile("x", 500)
	dirs := []*directory{
		&dirA,
		&dirB,
	}
	verify.Number(sumUpDirectorySizes(dirs)).Equal(1000).Assert(t)
}

func TestSumUpEmptyDirectoryListReturnsZero(t *testing.T) {
	verify.Number(sumUpDirectorySizes(make([]*directory, 0))).Equal(0).Assert(t)
}

func TestDirectorySizeReturnsSumOfFileSizesInDirectory(t *testing.T) {
	dir := CreateDirectory("test")
	dir.AddFile("a", 1)
	dir.AddFile("b", 2)
	dir.AddFile("c", 3)
	verify.Number(dir.Size()).Equal(6).Assert(t)
}

func TestDirectorySizeIsFileSizeWhenDirectoryOnlyContainsAFile(t *testing.T) {
	dir := CreateDirectory("test")
	dir.AddFile("a", 4711)
	verify.Number(dir.Size()).Equal(4711).Assert(t)
}

func TestDirectoryWithOneSubdirAndNoFilesReturnsSizeOfSubDirAsItsOwnSize(t *testing.T) {
	dir := CreateDirectory("test")
	subdir := CreateDirectory("subdir")
	subdir.AddFile("x", 512321321)
	dir.AddDirectory(&subdir)

	verify.Number(dir.Size()).Equal(subdir.Size()).Assert(t)
}

func TestSubdirectorySizeEqualsSumOfSizesOfSubdirectories(t *testing.T) {
	dir := CreateDirectory("test")
	subdir := CreateDirectory("subdir")
	subdir.AddFile("x", 512321321)
	dir.AddDirectory(&subdir)
	verify.Number(dir.subdirSize()).Equal(subdir.Size()).Assert(t)
}

func TestDirFileSizeEqualsSumOfSizeOfFilesInDirectory(t *testing.T) {
	dir := CreateDirectory("test")
	dir.AddFile("a", 4711)
	verify.Number(dir.fileSize()).Equal(4711).Assert(t)
}

func TestLsCommandAddsFilesWithSizesToDirectory(t *testing.T) {
	dir := CreateDirectory("test")

	parseLsCommandOutput(`
	14848514 b.txt
	8504156 c.dat`, &dir)

	verify.Number(len(dir.files)).Equal(2).Assert(t)
	verify.Number(dir.Size()).Equal(23352670).Assert(t)
}

func TestLsCommandAddsNewDirectory(t *testing.T) {
	dir := CreateDirectory("test")

	parseLsCommandOutput(`dir a
	dir b`, &dir)

	verify.Number(len(dir.subdirs)).Equal(2).Assert(t)
	verify.And(verify.True(dir.HasSubdir("a")), verify.True(dir.HasSubdir("b"))).Assert(t)
}

func TestDirectoryHasSubdirectoryAfterAddingADirectory(t *testing.T) {
	dir := CreateDirectory("x")
	subdir := CreateDirectory("y")
	dir.AddDirectory(&subdir)
	verify.True(dir.HasSubdir("y")).Assert(t)
}

func TestLSCommandInRootDirPopulatesRootWithFiles(t *testing.T) {
	fs := buildFileSystemFromCmdHistory(`$ ls
	1234 a.txt
	5679 b.txt`)

	verify.Number(fs.GetRoot().Size()).Equal(6913).Assert(t)
}

func TestCDCommandChangesCurrentFolder(t *testing.T) {
	fs := buildFileSystemFromCmdHistory(`$ ls
	dir x
	$ cd x`)

	verify.String(fs.CurrentDirectory().name).Equal("x").Assert(t)
}

func TestDirectoryCanBeFoundAfterAddingItToDirectory(t *testing.T) {
	dir := CreateDirectory("test")
	subdir := CreateDirectory("foo")
	dir.AddDirectory(&subdir)

	result, err := dir.FindDirectory("foo")
	verify.NoError(err).Assert(t)
	verify.String(result.name).Equal("foo").Assert(t)
}

func TestDirectoryCannotFindDirectoryThatWasNotAdded(t *testing.T) {
	dir := CreateDirectory("test")
	_, err := dir.FindDirectory("does not exist")
	verify.Error(err).Equal("directory does not exist").Assert(t)
}

func TestCDToSlashChangesDirectoryToRootDir(t *testing.T) {
	fs := buildFileSystemFromCmdHistory(`$ ls
	dir x
	$ cd /`)

	verify.True(fs.CurrentDirectory() == fs.GetRoot()).Assert(t)
}

func TestCDDotDotChangesToParentDirectory(t *testing.T) {
	fs := buildFileSystemFromCmdHistory(`$ ls
	dir x
	$ cd x
	$ cd ..`)

	verify.True(fs.CurrentDirectory() == fs.GetRoot()).Assert(t)
}

func TestDirectoyAddedAsSubdirectoryHasParentSet(t *testing.T) {
	dir := CreateDirectory("/")
	subdir := CreateDirectory("sub")
	dir.AddDirectory(&subdir)
	verify.True(subdir.Parent() == &dir).Assert(t)
}

func TestFindDirectoriesWithSizeReturnsListOfMatchingDirectories(t *testing.T) {
	fs := buildFileSystemFromCmdHistory(`$ ls
	dir x
	dir y
	$ cd x
	$ ls
	100 file1
	100 file 2
	$ cd ..
	$ cd y
	10 fileA
	20 fileB`)

	dirs := fs.FindDirectoriesWithSizeLessThan(100)
	verify.Number(len(dirs)).Equal(1).Assert(t)
	verify.Number(dirs[0].Size()).Equal(30).Assert(t)
}

func TestFindDirectoriesWithSizeReturnsListOfMatchingDirectoriesForNestedStructures(t *testing.T) {
	fs := buildFileSystemFromCmdHistory(`$ ls
	dir x
	dir y
	$ cd x
	$ ls
	100 file1
	100 file 2
	$ cd ..
	$ cd y
	10 fileA
	20 fileB
	dir a
	$ cd a
	$ ls
	5 file1234`)

	dirs := fs.FindDirectoriesWithSizeLessThan(100)
	verify.Number(len(dirs)).Equal(2).Assert(t)
}

func TestFindSmallestDirectoryThatFreesUpEnoughSpace(t *testing.T) {
	fs := buildFileSystemFromCmdHistory(`$ ls
	dir x
	dir y
	$ cd x
	$ ls
	100 file1
	100 file 2
	$ cd ..
	$ cd y
	10 fileA
	20 fileB
	dir thisIsTheDir
	dir a
	$ cd thisIsTheDir
	$ ls
	2 thesmallestfile
	$ cd ..
	$ cd a
	$ ls
	5 file1234`)

	result := fs.FindSmallestDirectoyToFree(1)
	verify.String(result.name).Equal("thisIsTheDir").Assert(t)
	verify.Number(result.Size()).Equal(2).Assert(t)
}
