package main

import (
	"testing"

	"github.com/fluentassert/verify"
)

func TestCalculateRoundScore(t *testing.T) {
	puzzle := NewDay2Puzzle()
	verify.Number(puzzle.CalculateRound(DAY2_OPPONENT_ROCK, DAY2_SELF_PAPER)).Equal(8).Assert(t)
	verify.Number(puzzle.CalculateRound(DAY2_OPPONENT_PAPER, DAY2_SELF_ROCK)).Equal(1).Assert(t)
	verify.Number(puzzle.CalculateRound(DAY2_OPPONENT_SICCORS, DAY2_SELF_SICCORS)).Equal(6).Assert(t)
}

func TestRockDefeatsSiccors(t *testing.T) {
	puzzle := NewDay2Puzzle()
	verify.Number(puzzle.EvaluateRound(DAY2_OPPONENT_SICCORS, DAY2_SELF_ROCK)).Equal(DAY2_WIN).Assert(t)
}

func TestSiccorsDefeatedByRock(t *testing.T) {
	puzzle := NewDay2Puzzle()
	verify.Number(puzzle.EvaluateRound(DAY2_OPPONENT_ROCK, DAY2_SELF_SICCORS)).Equal(DAY2_LOSS).Assert(t)
}

func TestPaperDefeatsRock(t *testing.T) {
	puzzle := NewDay2Puzzle()
	verify.Number(puzzle.EvaluateRound(DAY2_OPPONENT_ROCK, DAY2_SELF_PAPER)).Equal(DAY2_WIN).Assert(t)
}

func TestRockIsDefeatedByPaper(t *testing.T) {
	puzzle := NewDay2Puzzle()
	verify.Number(puzzle.EvaluateRound(DAY2_OPPONENT_PAPER, DAY2_SELF_ROCK)).Equal(DAY2_LOSS).Assert(t)
}

func TestSiccorsDefeatsPaper(t *testing.T) {
	puzzle := NewDay2Puzzle()
	verify.Number(puzzle.EvaluateRound(DAY2_OPPONENT_PAPER, DAY2_SELF_SICCORS)).Equal(DAY2_WIN).Assert(t)
}

func TestPaperIsDefeatedBySiccors(t *testing.T) {
	puzzle := NewDay2Puzzle()
	verify.Number(puzzle.EvaluateRound(DAY2_OPPONENT_SICCORS, DAY2_SELF_PAPER)).Equal(DAY2_LOSS).Assert(t)
}

func TestSelectingTheSameIsADraw(t *testing.T) {
	puzzle := NewDay2Puzzle()
	verify.Number(puzzle.EvaluateRound(DAY2_OPPONENT_SICCORS, DAY2_SELF_SICCORS)).Equal(DAY2_DRAW).Assert(t)
	verify.Number(puzzle.EvaluateRound(DAY2_OPPONENT_PAPER, DAY2_SELF_PAPER)).Equal(DAY2_DRAW).Assert(t)
	verify.Number(puzzle.EvaluateRound(DAY2_OPPONENT_ROCK, DAY2_SELF_ROCK)).Equal(DAY2_DRAW).Assert(t)
}

func TestCalculateRoundFromInputString(t *testing.T) {
	puzzle := NewDay2Puzzle()
	verify.Number(puzzle.CalculateRoundFromString("A Y")).Equal(4).Assert(t)
	verify.Number(puzzle.CalculateRoundFromString("B X")).Equal(1).Assert(t)
	verify.Number(puzzle.CalculateRoundFromString("C Z")).Equal(7).Assert(t)
}

func TestSumScoreFromString(t *testing.T) {
	puzzle := NewDay2Puzzle()
	score := puzzle.SumScoreFromString(`
	A Y
	B X
	C Z`)

	verify.Number(score).Equal(12).Assert(t)
}

func TestMapIntentToResult(t *testing.T) {
	puzzle := NewDay2Puzzle()
	verify.Number(puzzle.IntentToResult("Y")).Equal(DAY2_DRAW).Assert(t)
	verify.Number(puzzle.IntentToResult("X")).Equal(DAY2_LOSS).Assert(t)
	verify.Number(puzzle.IntentToResult("Z")).Equal(DAY2_WIN).Assert(t)
}

func TestPickSiccorsWhenOponentChoosesPaperAndIWantToWin(t *testing.T) {
	puzzle := NewDay2Puzzle()
	verify.String(puzzle.FigureOutWhatToChoose(DAY2_OPPONENT_PAPER, DAY2_WIN)).Equal(DAY2_SELF_SICCORS).Assert(t)
}

func TestPickPaperWhenOponentChoosesPaperAndIWantToDraw(t *testing.T) {
	puzzle := NewDay2Puzzle()
	verify.String(puzzle.FigureOutWhatToChoose(DAY2_OPPONENT_PAPER, DAY2_DRAW)).Equal(DAY2_SELF_PAPER).Assert(t)
}

func TestPickRockWhenOponentChoosesPaperAndIWantToLose(t *testing.T) {
	puzzle := NewDay2Puzzle()
	verify.String(puzzle.FigureOutWhatToChoose(DAY2_OPPONENT_PAPER, DAY2_LOSS)).Equal(DAY2_SELF_ROCK).Assert(t)
}

func TestPickRockWhenOponentChoosesSiccorsAndIWantToWin(t *testing.T) {
	puzzle := NewDay2Puzzle()
	verify.String(puzzle.FigureOutWhatToChoose(DAY2_OPPONENT_SICCORS, DAY2_WIN)).Equal(DAY2_SELF_ROCK).Assert(t)
}

func TestPickSiccorsWhenOponentChoosesSiccorsAndIWantToDraw(t *testing.T) {
	puzzle := NewDay2Puzzle()
	verify.String(puzzle.FigureOutWhatToChoose(DAY2_OPPONENT_SICCORS, DAY2_DRAW)).Equal(DAY2_SELF_SICCORS).Assert(t)
}

func TestPickPaperWhenOponentChoosesSiccorsAndIWantToLose(t *testing.T) {
	puzzle := NewDay2Puzzle()
	verify.String(puzzle.FigureOutWhatToChoose(DAY2_OPPONENT_SICCORS, DAY2_LOSS)).Equal(DAY2_SELF_PAPER).Assert(t)
}

func TestPickSiccorsWhenOponentChoosesRockAndIWantToLose(t *testing.T) {
	puzzle := NewDay2Puzzle()
	verify.String(puzzle.FigureOutWhatToChoose(DAY2_OPPONENT_ROCK, DAY2_LOSS)).Equal(DAY2_SELF_SICCORS).Assert(t)
}

func TestPickPaperWhenOponentChoosesRockAndIWantToWin(t *testing.T) {
	puzzle := NewDay2Puzzle()
	verify.String(puzzle.FigureOutWhatToChoose(DAY2_OPPONENT_ROCK, DAY2_WIN)).Equal(DAY2_SELF_PAPER).Assert(t)
}

func TestPickRockWhenOponentChoosesRockAndIWantToDraw(t *testing.T) {
	puzzle := NewDay2Puzzle()
	verify.String(puzzle.FigureOutWhatToChoose(DAY2_OPPONENT_ROCK, DAY2_DRAW)).Equal(DAY2_SELF_ROCK).Assert(t)
}
