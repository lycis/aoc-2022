package main

import (
	"aoc-2022/helper"
	"fmt"
	"strconv"
	"strings"

	"github.com/akrennmair/slice"
)

func CalculateTotalSizeOfDirectoriesFromInput(input string) int {
	fs := buildFileSystemFromCmdHistory(input)
	dirs := fs.FindDirectoriesWithSizeLessThan(100000)
	return sumUpDirectorySizes(dirs)
}

func buildFileSystemFromCmdHistory(input string) virtualFileSystem {
	fs := CreateVirtualFileSystem()
	mode := 0

	helper.ProcessStringLineByLine(input, func(line string) {
		line = strings.Trim(strings.TrimSpace(line), "\t")
		if strings.HasPrefix(line, "$ ") {
			parts := strings.Split(line, " ")
			switch parts[1] {
			case "ls":
				mode = 1
			case "cd":
				var targetDir *directory

				if parts[2] == "/" {
					targetDir = fs.GetRoot()
				} else if parts[2] == ".." {
					targetDir = fs.CurrentDirectory().Parent()
				} else {
					var err error
					targetDir, err = fs.CurrentDirectory().FindDirectory(parts[2])
					if err != nil {
						panic(err)
					}
				}
				fs.cwd = targetDir

			}
		} else if mode == 1 {
			parseLsCommandOutput(line, fs.CurrentDirectory())
		}
	})

	return fs
}

func parseLsCommandOutput(lsOutput string, currentDir *directory) {
	helper.ProcessStringLineByLine(lsOutput, func(line string) {
		s := strings.Trim(strings.TrimSpace(line), "\t")
		if s == "" {
			return
		}

		if strings.HasPrefix(s, "dir ") {
			parts := strings.Split(s, " ")
			dir := CreateDirectory(parts[1])
			currentDir.AddDirectory(&dir)
		} else {
			parts := strings.Split(s, " ")
			size, _ := strconv.ParseInt(parts[0], 10, 32)
			name := parts[1]
			currentDir.AddFile(name, int(size))
		}
	})
}

type virtualFileSystem struct {
	root *directory
	cwd  *directory
}

func CreateVirtualFileSystem() virtualFileSystem {
	rootDir := CreateDirectory("/")
	fs := virtualFileSystem{
		root: &rootDir,
		cwd:  &rootDir,
	}
	return fs
}

func (fs virtualFileSystem) GetRoot() *directory {
	return fs.root
}

func (fs virtualFileSystem) FindDirectoriesWithSizeLessThan(sizeLimit int) []*directory {
	list := make([]*directory, 0)
	fs.scanDir(fs.GetRoot(), func(dir *directory) {
		if dir.Size() <= sizeLimit {
			list = append(list, dir)
		}
	})
	return list
}

func (fs virtualFileSystem) scanDir(cwd *directory, fx func(*directory)) {
	for _, dir := range cwd.subdirs {
		fs.scanDir(dir, fx)
		fx(dir)
	}
}

/*
func (fs virtualFileSystem) scanDir(cwd *directory, sizeLimit int, list []*directory) []*directory {
	for _, dir := range cwd.subdirs {
		list = fs.scanDir(dir, sizeLimit, list)
		if dir.Size() <= sizeLimit {
			list = append(list, dir)
		}
	}
	return list
}*/

func (fs virtualFileSystem) FindSmallestDirectoyToFree(minSize int) *directory {
	var targetDir *directory

	fs.scanDir(fs.GetRoot(), func(dir *directory) {
		if dir.Size() < minSize {
			return
		}

		if targetDir == nil {
			targetDir = dir
		} else if dir.Size() < targetDir.Size() {
			targetDir = dir
		}
	})

	return targetDir
}

func (fs virtualFileSystem) CurrentDirectory() *directory {
	return fs.cwd
}

func CreateDirectory(name string) directory {
	return directory{
		name:    name,
		files:   make(map[string]int),
		subdirs: make([]*directory, 0),
	}
}

type directory struct {
	name    string
	files   map[string]int
	subdirs []*directory
	parent  *directory
}

func (d directory) Parent() *directory {
	return d.parent
}

func (d directory) Size() int {
	return d.fileSize() + d.subdirSize()
}

func (d directory) fileSize() int {
	return slice.Reduce(slice.Map(helper.Keys(d.files), func(key string) int { return d.files[key] }), func(total, i int) int { return total + i })
}

func (d directory) subdirSize() int {
	return slice.Reduce(slice.Map(d.subdirs, func(d *directory) int { return d.Size() }), func(t, i int) int { return t + i })
}

func (d *directory) AddFile(name string, size int) {
	d.files[name] = size
}

func (d *directory) AddDirectory(subdir *directory) {
	subdir.parent = d
	d.subdirs = append(d.subdirs, subdir)
}

func (d directory) HasSubdir(name string) bool {
	for _, d := range d.subdirs {
		if d.name == name {
			return true
		}
	}
	return false
}

func (d directory) FindDirectory(name string) (*directory, error) {
	for _, dir := range d.subdirs {
		if dir.name == name {
			return dir, nil
		}
	}

	return nil, fmt.Errorf("directory does not exist")
}

func sumUpDirectorySizes(dlist []*directory) int {
	return slice.Reduce(
		slice.Map(dlist, func(d *directory) int { return d.Size() }),
		func(total, i int) int { return total + i })
}

type Day7Result struct {
	input string
}

func (r Day7Result) Result() string {
	fs := buildFileSystemFromCmdHistory(r.input)
	freeSize := 70000000 - fs.GetRoot().Size()
	requiredtoFree := 30000000 - freeSize
	return fmt.Sprintf("sumOfSizesBelow100k=%d smallestDirSizeTodelete=%d", sumUpDirectorySizes(fs.FindDirectoriesWithSizeLessThan(100000)), fs.FindSmallestDirectoyToFree(requiredtoFree).Size())
}

func Day7Runner(input string) dayResult {
	return Day7Result{input}
}
