package helper

import (
	"reflect"
)

func Keys(v interface{}) []string {
	rv := reflect.ValueOf(v)
	if rv.Kind() != reflect.Map {
		return nil
	}
	t := rv.Type()
	if t.Key().Kind() != reflect.String {
		return nil
	}
	var result []string
	for _, kv := range rv.MapKeys() {
		result = append(result, kv.String())
	}
	return result
}
