package helper

import (
	"testing"

	"github.com/fluentassert/verify"
)

func TestProcessGivenInputStringLineByLineAndCallGivenFunction(t *testing.T) {
	input := `a
	b
	c
	d`

	n := 0
	ProcessStringLineByLine(input, func(line string) {
		n++
	})

	verify.Number(n).Equal(4).Assert(t)
}
