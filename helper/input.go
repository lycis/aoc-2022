package helper

import (
	"bufio"
	"strings"
)

type processingFunc func(string)

func ProcessStringLineByLine(input string, f processingFunc) {
	scanner := bufio.NewScanner(strings.NewReader(input))
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		f(scanner.Text())
	}
}
