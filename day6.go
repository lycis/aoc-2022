package main

import (
	"fmt"
	"strings"
)

func FindStartOfCommunicationPackage(input string) int {
	return searchForCommunicationMarker(input, 4)
}

func FindStartOfCommunicationMessage(input string) int {
	return searchForCommunicationMarker(input, 14)
}

func searchForCommunicationMarker(input string, markerLength int) int {
	num := -1
	breaker := false
	processSlidingWindowString(input, markerLength, func(str string) {
		if breaker {
			return
		}

		num++
		if allCharactersAreDifferent(str) {
			breaker = true
		}
	})
	return num + markerLength
}

func processSlidingWindowString(str string, windowSize int, fx func(string)) {
	size := windowSize
	for i := 0; i+size <= len(str); i++ {
		fx(str[i : i+size])
	}
}

func allCharactersAreDifferent(str string) bool {
	for i := 0; i < len(str); i++ {
		if strings.Count(str, string(str[i])) > 1 {
			return false
		}
	}

	return true
}

type Day6Result struct {
	input string
}

func (p Day6Result) Result() string {
	return fmt.Sprintf("packageStartsAt=%d messageStartsAt=%d", FindStartOfCommunicationPackage(p.input), FindStartOfCommunicationMessage(p.input))
}

func Day6Runner(input string) dayResult {
	return Day6Result{input}
}
