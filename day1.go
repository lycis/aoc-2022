package main

import (
	"bufio"
	"fmt"
	"sort"
	"strconv"
	"strings"
)

type aocDay1 struct {
	currentBucket int
	buckets       []int
}

func (p aocDay1) Result() string {
	bucket := p.FindLargestBucket()
	return fmt.Sprintf("highestCalories=%d elf=%d topThree=%d", p.buckets[bucket], bucket, p.CalcTopThree())
}

func (p *aocDay1) openBucket() {
	p.buckets = append(p.buckets, 0)
	p.currentBucket++
}

func (p *aocDay1) AddToBucket(n int) {
	p.buckets[p.currentBucket] += n
}

func (p aocDay1) FindLargestBucket() int {
	largest := 0

	for i, sum := range p.buckets {
		if sum > p.buckets[largest] {
			largest = i
		}
	}

	return largest
}

func (p *aocDay1) CalcTopThree() int {
	sort.Sort(sort.Reverse(sort.IntSlice(p.buckets)))

	return p.buckets[0] + p.buckets[1] + p.buckets[2]
}

func NewDay1Puzzle() aocDay1 {
	var p aocDay1
	p.currentBucket = -1
	return p
}

func Day1(input string) aocDay1 {
	p := NewDay1Puzzle()

	scanner := bufio.NewScanner(strings.NewReader(input))
	scanner.Split(bufio.ScanLines)

	var lastLineProcessed string
	p.openBucket()
	for scanner.Scan() {
		lastLineProcessed = strings.TrimSpace(scanner.Text())
		if lastLineProcessed == "" {
			p.openBucket()
		} else {
			n, _ := strconv.ParseInt(lastLineProcessed, 10, 64)
			p.AddToBucket(int(n))
		}
	}

	return p
}

func Day1Runner(input string) dayResult {
	return Day1(input)
}
