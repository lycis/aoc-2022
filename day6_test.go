package main

import (
	"testing"

	"github.com/fluentassert/verify"
)

func TestFindStartOfPackage(t *testing.T) {
	verify.Number(FindStartOfCommunicationPackage("mjqjpqmgbljsphdztnvjfqwrcgsmlb")).Equal(7).Assert(t)
	verify.Number(FindStartOfCommunicationPackage("bvwbjplbgvbhsrlpgdmjqwftvncz")).Equal(5).Assert(t)
	verify.Number(FindStartOfCommunicationPackage("nppdvjthqldpwncqszvftbrmjlhg")).Equal(6).Assert(t)
	verify.Number(FindStartOfCommunicationPackage("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")).Equal(10).Assert(t)
	verify.Number(FindStartOfCommunicationPackage("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")).Equal(11).Assert(t)
}

func TestFindStartOMessage(t *testing.T) {
	verify.Number(FindStartOfCommunicationMessage("mjqjpqmgbljsphdztnvjfqwrcgsmlb")).Equal(19).Assert(t)
	verify.Number(FindStartOfCommunicationMessage("bvwbjplbgvbhsrlpgdmjqwftvncz")).Equal(23).Assert(t)
	verify.Number(FindStartOfCommunicationMessage("nppdvjthqldpwncqszvftbrmjlhg")).Equal(23).Assert(t)
	verify.Number(FindStartOfCommunicationMessage("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")).Equal(29).Assert(t)
	verify.Number(FindStartOfCommunicationMessage("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")).Equal(26).Assert(t)
}

func TestProcessSlidingWindowString(t *testing.T) {
	result := ""
	processSlidingWindowString("abcdefg", 4, func(window string) {
		result += window
	})
	verify.String(result).Equal("abcdbcdecdefdefg").Assert(t)
}

func TestAllCharactersInStringAreDifferentIsTrueWhenAllAreDifferent(t *testing.T) {
	verify.True(allCharactersAreDifferent("abcdefghijkl")).Assert(t)
}

func TestAllCharactersAreDifferentIsFalseWhenAtLeastOneCharacterIsSame(t *testing.T) {
	verify.False(allCharactersAreDifferent("abcdd")).Assert(t)
	verify.False(allCharactersAreDifferent("abddd")).Assert(t)
	verify.False(allCharactersAreDifferent("adddd")).Assert(t)
}
