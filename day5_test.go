package main

import (
	"testing"

	"github.com/fluentassert/verify"
)

func TestFigureOutTopCratesFromInput(t *testing.T) {
	result := Day5RearrangeCratesAccordingToInstructions(`    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2`)

	verify.String(result.Result()).Equal("MCD").Assert(t)
}

func TestInitialSetupReturnsInitialCratesOnTop(t *testing.T) {
	result := Day5RearrangeCratesAccordingToInstructions(`    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3`)

	verify.String(result.Result()).Equal("NDP").Assert(t)
}

func TestPutCratePushesACrateToTopOfEmptyStack(t *testing.T) {
	puzzle := NewDay5Puzzle()
	puzzle.PutCrate("X", 1)

	verify.String(puzzle.GetTopCrateOfStack(1)).Equal("X").Assert(t)
}

func TestPuttingASecondCrateOnAStackReturnsLatestAsTopCrate(t *testing.T) {
	puzzle := NewDay5Puzzle()

	puzzle.PutCrate("A", 0)
	puzzle.PutCrate("B", 0)

	verify.String(puzzle.GetTopCrateOfStack(0)).Equal("B").Assert(t)
}

func TestPuttingTwoCratesOnTwoDifferentStacksReturnsEachOfThemForTopOfTheirStack(t *testing.T) {
	puzzle := NewDay5Puzzle()

	puzzle.PutCrate("A", 1)
	puzzle.PutCrate("B", 2)

	verify.String(puzzle.GetTopCrateOfStack(1)).Equal("A").Assert(t)
	verify.String(puzzle.GetTopCrateOfStack(2)).Equal("B").Assert(t)
}

func TestMovingCrateFromOneStackToAnotherRevealsLowerCratesOnStack(t *testing.T) {
	puzzle := NewDay5Puzzle()
	puzzle.PutCrate("X", 1)
	puzzle.PutCrate("Y", 1)
	puzzle.PutCrate("A", 0)
	puzzle.PutCrate("B", 0)

	verify.String(puzzle.GetTopCrateOfStack(1)).Equal("Y").Assert(t)
	verify.String(puzzle.GetTopCrateOfStack(0)).Equal("B").Assert(t)

	puzzle.MoveCrate(1, 0)

	verify.String(puzzle.GetTopCrateOfStack(1)).Equal("X").Assert(t)
	verify.String(puzzle.GetTopCrateOfStack(0)).Equal("Y").Assert(t)
}

func TestExtractMoveCrateCommand(t *testing.T) {
	num, from, to := TokenizeMoveCrateCommand("move 3 from 1 to 4")

	verify.Number(num).Equal(3).Assert(t)
	verify.Number(from).Equal(1).Assert(t)
	verify.Number(to).Equal(4).Assert(t)
}

func TestMoveCratesInSameOrder(t *testing.T) {
	puzzle := NewDay5Puzzle()
	puzzle.PutCrate("X", 1)
	puzzle.PutCrate("Y", 1)
	puzzle.PutCrate("Z", 1)
	puzzle.PutCrate("A", 0)
	puzzle.PutCrate("B", 0)

	puzzle.MoveCrateStack(2, 1, 0)

	verify.String(puzzle.GetTopCrateOfStack(1)).Equal("X").Assert(t)
	verify.String(puzzle.GetTopCrateOfStack(0)).Equal("Z").Assert(t)
}
