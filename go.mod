module aoc-2022

go 1.19

require github.com/fluentassert/verify v1.0.0-rc.1 // direct

require (
	github.com/akrennmair/slice v0.0.0-20220105203817-49445747ab81 // indirect
	github.com/deckarep/golang-set/v2 v2.1.0 // indirect
	github.com/google/go-cmp v0.5.9 // indirect
)
